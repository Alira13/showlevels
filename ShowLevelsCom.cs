﻿using System;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.Attributes;
using System.Collections.Generic;
using ShowLevels.Model;
using ShowLevels.ViewModel;
using ShowLevels.View;

namespace ShowLevels
{
    [TransactionAttribute(TransactionMode.ReadOnly)]

    public class ShowLevelsCom : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIApplication rvtUIApp = commandData.Application;
            UIDocument rvtUIDoc = rvtUIApp.ActiveUIDocument;

            ShowLevelsModel.CurrentDocument = rvtUIApp.ActiveUIDocument.Document;


            ShowLevelsView showLevelsView = new ShowLevelsView();
            showLevelsView.Show();

            return Result.Succeeded;
        }
    }
}
