﻿using System.Collections.Generic;
using System.ComponentModel;
using ShowLevels.Model;

namespace ShowLevels.ViewModel
{
    public class ShowLevelsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICollection<LevelClass> LevelsInDoc { get; private set; }

        public ShowLevelsViewModel()
        {
            LevelsInDoc = ShowLevelsModel.GetLevels();
        }

    }
}
