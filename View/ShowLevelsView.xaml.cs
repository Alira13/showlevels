﻿using System.Windows;

namespace ShowLevels.View
{
    /// <summary>
    /// Логика взаимодействия для ShowLevelsView.xaml
    /// </summary>
    public partial class ShowLevelsView : Window
    {
        public ShowLevelsView()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ShowLevelsWindow.Close();
        }

    }
}
