﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;


namespace ShowLevels.Model
{
    public class LevelClass : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        string levelHeight;
        public string LevelHeight
        {
            get { return levelHeight; }
            set
            {
                levelHeight = value;
                OnPropertyChanged("levelHeight");
            }
        }


        string levelName;
        public string LevelName
        {
            get { return levelName; }
            set
            {
                levelName = value;
                OnPropertyChanged("levelName"); 
            }
        }
    }

    public class ShowLevelsModel
    {
       
        public static Document CurrentDocument { get; set; }

        public static ICollection<LevelClass> GetLevels()
        {
            FilteredElementCollector collector = new FilteredElementCollector(CurrentDocument);
            ICollection<Element> rvtLevelsCollection = collector.OfClass(typeof(Level)).ToElements();
            ICollection<LevelClass> levelsCollection = new List <LevelClass>();

            foreach (Element e in rvtLevelsCollection)
            {
                Level level = e as Level;
                if (level!=null)
                {
                    var heightInMilimiters = UnitUtils.ConvertFromInternalUnits(level.Elevation, DisplayUnitType.DUT_MILLIMETERS).ToString();
                    levelsCollection.Add(new LevelClass() { LevelName = level.Name, LevelHeight= heightInMilimiters});
                }
            }
            return levelsCollection;
        }
    }
}
